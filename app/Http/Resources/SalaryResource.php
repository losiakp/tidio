<?php

namespace App\Http\Resources;

use App\Models\Salary;
use App\Services\Money\Money;
use Illuminate\Http\Resources\Json\JsonResource;

class SalaryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'first_name' => $this->employee->first_name,
            'last_name'  => $this->employee->last_name,
            'department' => $this->employee->department->name,
            'base'       => Money::format($this->base),
            'bonus'      => Money::format($this->bonus),
            'bonus_type' => Salary::BONUS_TYPE_LABELS[$this->bonus_type],
            'total'      => Money::format($this->total),
        ];
    }
}
