<?php

namespace App\Http\Controllers;

use App\Factories\EmployeeFactory;
use App\Models\Employee;
use Illuminate\Http\Request;

class EmployeesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return Employee::applyFilters($request->all())->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $departmentId = $request->get('department_id');
        $firstName = $request->get('first_name');
        $lastName = $request->get('last_name');
        $baseSalary = $request->get('base_salary');
        $employmentDate = $request->get('employment_date');

        $employeeFactory = new EmployeeFactory();
        $employee = $employeeFactory->create($departmentId, $firstName, $lastName, $baseSalary, $employmentDate);

        return response()->json([
            'employee' => $employee
        ]);
    }
}
