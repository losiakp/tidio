<?php

namespace App\Http\Controllers;

use App\Http\Resources\SalaryResource;
use App\Models\Salary;
use Illuminate\Http\Request;

class SalariesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     */
    public function index(Request $request)
    {
        $salaries = Salary::applyFilters($request->all())->get();

        return SalaryResource::collection($salaries)->toArray($request);
    }
}
