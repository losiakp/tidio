<?php

namespace App\Factories;

use App\Models\Employee;

final class EmployeeFactory
{
    /**
     * @param $departmentId
     * @param $firstName
     * @param $lastName
     * @param $baseSalary
     * @param null $employmentDate
     * @return Employee
     */
    public function create($departmentId, $firstName, $lastName, $baseSalary, $employmentDate = null): Employee
    {
        $employee = new Employee();
        $employee->department_id = $departmentId;
        $employee->first_name = $firstName;
        $employee->last_name = $lastName;
        $employee->employment_date = $employmentDate ?? now();
        $employee->save();

        $salaryFactory = new SalaryFactory();
        $salaryFactory->create($employee, $baseSalary);

        return $employee;
    }
}
