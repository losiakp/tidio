<?php

namespace App\Factories;

use App\Models\Salary;
use App\Services\SalaryCalculator\CalculateSalaryTotal;

final class SalaryFactory
{
    /**
     * @param $employee
     * @param $base
     * @return Salary
     */
    public function create($employee, $base)
    {
        $salary = new Salary();
        $salary->base = $base;
        $salary->employee_id = $employee->id;
        $calculateSalary = new CalculateSalaryTotal($salary);
        $calculateSalary->handle();
        $salary->save();

        return $salary;
    }
}
