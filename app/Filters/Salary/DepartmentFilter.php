<?php

namespace App\Filters\Salary;

use App\Services\Filters\Contracts\Filter;
use Illuminate\Database\Eloquent\Builder;

class DepartmentFilter implements Filter
{
    /**
     * @param $query
     * @param $values
     * @return mixed
     */
    public function handle(Builder $query, $values)
    {
        return $query->whereHas('employee.department', function ($query) use ($values) {
            $query->simpleSearch($values[0], [
                'name',
            ]);
        });
    }
}
