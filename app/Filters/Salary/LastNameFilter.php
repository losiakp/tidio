<?php

namespace App\Filters\Salary;


use App\Services\Filters\Contracts\Filter;
use Illuminate\Database\Eloquent\Builder;

class LastNameFilter implements Filter
{
    /**
     * @param $query
     * @param $values
     * @return mixed
     */
    public function handle(Builder $query, $values)
    {
        return $query->whereHas('employee', function ($query) use ($values) {
            $query->simpleSearch($values[0], [
                'last_name',
            ]);
        });
    }
}
