<?php

namespace App\Filters\Salary;

use App\Services\Filters\Contracts\Filter;
use Illuminate\Database\Eloquent\Builder;

class OrderByFilter implements Filter
{
    /**
     * @param Builder $query
     * @param $values
     * @return Builder
     */
    public function handle(Builder $query, $values): Builder
    {
        $column = $values[0];
        $direction = $values[1] ?? 'asc';

        $query->join(\DB::raw("(select * from employees) employees") , "salaries.employee_id", '=', 'employees.id');
        $query->join(\DB::raw("(select * from departments) departments") , "employees.department_id", '=', 'departments.id');

        $query->orderBy($column, $direction);

        return $query;
    }
}
