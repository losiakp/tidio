<?php

namespace App\Filters\Employee;


use App\Services\Filters\Contracts\Filter;
use Illuminate\Database\Eloquent\Builder;

class NameFilter implements Filter
{
    /**
     * @param $query
     * @param $values
     * @return mixed
     */
    public function handle(Builder $query, $values)
    {
        return $query->simpleSearch($values[0], [
            'first_name',
            'last_name',
        ]);
    }
}
