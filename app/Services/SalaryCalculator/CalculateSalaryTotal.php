<?php

namespace App\Services\SalaryCalculator;

use App\Models\Department;
use App\Models\Employee;
use App\Models\Salary;

class CalculateSalaryTotal
{
    private Salary $salary;
    private Employee $employee;
    private Department $department;
    private int $maxSeniority = 10;

    public function __construct(Salary $salary)
    {
        $this->salary = $salary;
        $this->employee = $salary->employee;
        $this->department = $this->employee->department;
    }

    public function handle()
    {
        $this->calculateAmountBonus();
        $this->calculatePercentBonus();

        $this->calculateTotal();
        $this->salary->save();
    }

    private function countSeniority()
    {
        return now()->diffInYears($this->employee->employment_date);
    }

    private function calculatePercentBonus()
    {
        if ($this->department->salary_bonus_percent) {
            $this->salary->bonus = $this->department->salary_bonus_percent / 100 * $this->salary->base;
            $this->salary->bonus_type = Salary::BONUS_TYPE_PERCENT;
        }
    }

    private function calculateAmountBonus()
    {
        if ($this->department->salary_bonus_amount) {
            $seniority = $this->countSeniority();

            if ($seniority > $this->maxSeniority) {
                $seniority = $this->maxSeniority;
            }

            $this->salary->bonus = $seniority * $this->department->salary_bonus_amount;
            $this->salary->bonus_type = Salary::BONUS_TYPE_AMOUNT;
        }
    }

    private function calculateTotal()
    {
        $this->salary->total = $this->salary->base + $this->salary->bonus;
    }
}
