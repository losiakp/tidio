<?php

namespace App\Services\Money;

final class Money
{
    public static function format($amount)
    {
        return $amount / 100;
    }
}
