<?php

namespace App\Services\Filters\Orderable;

interface OrderableItemInterface
{
    public function getTable(): string;
    public function getRelatedTable(): string;
    public function getForeignKey(): string;
}
