<?php

namespace App\Services\Filters\Drivers;

use App\Services\Filters\Contracts\Filter;
use App\Services\Filters\Contracts\FiltersDriver;
use App\Services\Filters\Contracts\FiltersKernel;
use App\Services\Filters\FilterBuilder;
use App\Services\Filters\RelationFiltersParser;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Str;

class EloquentDriver implements FiltersDriver
{
    /**
     * @var Builder
     */
    private $builder;

    /**
     * @var FiltersKernel
     */
    private $kernel;

    /**
     * EloquentDriver constructor.
     * @param Builder $builder
     * @param FiltersKernel $kernel
     */
    public function __construct(Builder $builder, FiltersKernel $kernel)
    {
        $this->builder = $builder;
        $this->kernel = $kernel;
    }

    /**
     * @param Filter $filter
     * @param $values
     * @return mixed
     */
    public function apply(Filter $filter, $values)
    {
        return $filter->handle($this->builder, $values);
    }

    /**
     * @param $includes
     * @return array
     */
    public function applyRelations($includes): array
    {
        $relationFilterParser = new RelationFiltersParser($includes);
        $relationFilterParser->parse();

        $includeParams = $relationFilterParser->getIncludeParams();
        $relations = collect();

        if (is_array($includeParams)) {
            foreach ($includeParams as $key => $relationFilters) {
                $filterBuilder = new FilterBuilder();
                $keyArray = explode('.',$key);
                $module = Str::camel(Str::singular(end($keyArray)));

                $relations->put($key, function (Relation $query) use ($relationFilters, $module, $filterBuilder) {
                    foreach ($relationFilters as $name => $values) {
                        $filterBuilder
                            ->setName($name)
                            ->setModule($this->kernel->getFilterModules()[$module]);

                        $filter = $filterBuilder->build();

                        if (is_array($values) && count($values)) {
                            $filter->handle($query->getQuery(), $values);
                        }
                    }
                });
            }
        }

        return $relations->toArray();
    }

    /**
     * @return FiltersKernel
     */
    public function getKernel(): FiltersKernel
    {
        return $this->kernel;
    }
}
