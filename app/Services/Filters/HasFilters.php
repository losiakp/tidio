<?php

namespace App\Services\Filters;

use App\Services\Filters\Drivers\EloquentDriver;
use Illuminate\Database\Eloquent\Builder;

trait HasFilters
{
    /**
     * Filter results
     *
     * @param Builder $builder
     * @param array $filters
     * @return Builder
     * @throws Exceptions\FilterDoesNotExistException
     */
    public function scopeApplyFilters(Builder $builder, array $filters)
    {
        $processor = new FiltersProcessor(
            new EloquentDriver($builder, new Kernel()),
            $filters,
            mb_strtolower(class_basename($this))
        );

        $processor->process();

        return $builder->with($processor->applyRelationFilters());
    }
}
