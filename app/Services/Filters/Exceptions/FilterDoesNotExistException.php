<?php

namespace App\Services\Filters\Exceptions;

use Exception;

class FilterDoesNotExistException extends Exception
{

}
