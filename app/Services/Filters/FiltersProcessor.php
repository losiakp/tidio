<?php

namespace App\Services\Filters;

use App\Services\Filters\Contracts\FiltersDriver;
use App\Services\Filters\Contracts\FiltersKernel;
use App\Services\Filters\Exceptions\FilterDoesNotExistException;
use Illuminate\Database\Eloquent\Relations\Relation;

class FiltersProcessor
{
    /**
     * @var array
     */
    protected $filters;

    /**
     * @var FiltersDriver
     */
    protected $driver;

    /**
     * @var string
     */
    private $module;

    /**
     * @var array
     */
    protected $relations = [];

    /**
     * FiltersProcessor constructor.
     * @param FiltersDriver $driver
     * @param array $filters
     * @param $module
     */
    public function __construct(FiltersDriver $driver, array $filters, string $module)
    {
        $this->filters = $filters;
        $this->driver = $driver;
        $this->module = $module;
    }

    /**
     * @throws FilterDoesNotExistException
     */
    public function process()
    {
        foreach ($this->filters as $name => $values) {
            if ($name === 'include') {
                continue;
            }

            $filterBuilder = new FilterBuilder();
            $filterBuilder
                ->setName($name)
                ->setModule($this->driver->getKernel()->getFilterModules()[$this->module]);

            $filter = $filterBuilder->build();

            $this->driver->apply($filter, explode('|', $values));
        }

        if(array_key_exists('include', $this->filters)) {
            $this->relations = $this->driver->applyRelations($this->filters['include']);
        }
    }

    /**
     * @return array
     */
    public function applyRelationFilters(): array
    {
        return $this->relations;
    }
}
