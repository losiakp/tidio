<?php

namespace App\Services\Filters\Contracts;

interface FiltersDriver
{
    public function apply(Filter $filter, $values);
    public function applyRelations($includes): array;
    public function getKernel(): FiltersKernel;
}
