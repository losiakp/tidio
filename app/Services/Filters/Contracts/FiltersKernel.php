<?php

namespace App\Services\Filters\Contracts;

interface FiltersKernel
{
    public function getFilterModules();
}
