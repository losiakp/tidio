<?php

namespace App\Services\Filters\Contracts;

use Illuminate\Database\Eloquent\Builder;

interface Filter
{
    public function handle(Builder $query, $values);
}
