<?php

namespace App\Services\Filters;

use App\Services\Filters\Contracts\Filter;
use App\Services\Filters\Exceptions\FilterDoesNotExistException;

class FilterBuilder
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $module;

    /**
     * @return Filter
     * @throws FilterDoesNotExistException
     */
    public function build(): Filter
    {
        $filterClass = $this->module . '\\' . ucfirst($this->name) . 'Filter';

        if (! class_exists($filterClass)) {
            throw new FilterDoesNotExistException('Filter missing in ' . $filterClass);
        }

        return new $filterClass();
    }

    /**
     * @param mixed $name
     * @return FilterBuilder
     */
    public function setName(string $name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @param mixed $module
     * @return FilterBuilder
     */
    public function setModule(string $module)
    {
        $this->module = $module;

        return $this;
    }
}
