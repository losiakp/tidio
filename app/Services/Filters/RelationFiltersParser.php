<?php

namespace App\Services\Filters;

class RelationFiltersParser
{
    /**
     * @var mixed
     */
    private $includes;

    /**
     * @var array
     */
    private $requestedIncludes = [];

    /**
     * @var array
     */
    private $includeParams = [];

    /**
     * RelationFiltersParser constructor.
     * @param $includes
     */
    public function __construct($includes)
    {
        $this->includes = $includes;
    }

    /**
     * @return bool
     */
    public function parse(): bool
    {
        if (is_string($this->includes)) {
            $this->includes = explode(',', $this->includes);
        }

        if (! is_array($this->includes)) {
            throw new \InvalidArgumentException(
                'The parseIncludes() method expects a string or an array. ' . gettype($this->includes) . ' given'
            );
        }

        foreach ($this->includes as $include) {
            list($includeName, $allModifiersStr) = array_pad(explode(':', $include, 2), 2, null);

            $includeName = $this->trimToAcceptableRecursionLevel($includeName);

            if (in_array($includeName, $this->requestedIncludes)) {
                continue;
            }
            $this->requestedIncludes[] = $includeName;

            // No Params? Bored
            if ($allModifiersStr === null) {
                continue;
            }

            // Matches multiple instances of 'something(foo|bar|baz)' in the string
            // I guess it ignores : so you could use anything, but probably don't do that
            preg_match_all('/([\w]+)(\(([^\)]+)\))?/', $allModifiersStr, $allModifiersArr);

            // [0] is full matched strings...
            $modifierCount = count($allModifiersArr[0]);

            $modifierArray = [];

            for ($modifierIt = 0; $modifierIt < $modifierCount; $modifierIt++) {
                // [1] is the modifier
                $modifierName = $allModifiersArr[1][$modifierIt];

                // and [3] is delimited params
                $modifierParamStr = $allModifiersArr[3][$modifierIt];

                // Make modifier array key with an array of params as the value
                $modifierArray[$modifierName] = explode('|', $modifierParamStr);
            }

            $this->includeParams[$includeName] = $modifierArray;
        }

        return true;
    }

    /**
     * Trim to Acceptable Recursion Level
     *
     * Strip off any requested resources that are too many levels deep, to avoid DiCaprio being chased
     * by trains or whatever the hell that movie was about.
     *
     * @internal
     *
     * @param string $includeName
     *
     * @return string
     */
    protected function trimToAcceptableRecursionLevel($includeName)
    {
        return implode('.', array_slice(explode('.', $includeName), 0, 3));
    }

    /**
     * @return mixed
     */
    public function getIncludes()
    {
        return $this->includes;
    }

    /**
     * @return array
     */
    public function getRequestedIncludes(): array
    {
        return $this->requestedIncludes;
    }

    /**
     * @return array
     */
    public function getIncludeParams(): array
    {
        return $this->includeParams;
    }
}
