<?php

namespace App\Services\Filters;

use App\Services\Filters\Contracts\FiltersKernel;

class Kernel implements FiltersKernel
{
    /**
     * @var array
     */
    protected $filters = [
        'employee' => 'App\\Filters\\Employee',
        'salary' => 'App\\Filters\\Salary',
    ];

    /**
     * @return array
     */
    public function getFilterModules()
    {
        return $this->filters;
    }
}
