<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Builder;

trait Findable
{
    public function scopeSimpleSearch(Builder $builder, $search, $data)
    {
        $searchParts = explode(' ', $search);

        foreach ($searchParts as $part) {
            $builder->where(function ($query) use ($part, $data) {
                foreach ($data as $column) {
                    $query->orWhere($column, 'like', '%' . $part . '%');
                }
            });
        }

        return $builder;
    }
}
