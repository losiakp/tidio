<?php

namespace App\Models;

use App\Services\Filters\HasFilters;
use App\Traits\Findable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Salary extends Model
{
    use HasFactory, HasFilters, Findable;

    public const BONUS_TYPE_AMOUNT = 1;
    public const BONUS_TYPE_PERCENT = 2;

    public const BONUS_TYPE_LABELS = [
        1 => 'Amount',
        2 => 'Percent',
    ];

    protected $fillable = [
        'employee_id',
        'base',
        'bonus',
        'bonus_type',
        'total',
    ];

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }
}
