<?php

namespace App\Models;

use App\Services\Filters\HasFilters;
use App\Traits\Findable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    use HasFactory, HasFilters, Findable;

    protected $fillable = [
        'department_id',
        'first_name',
        'last_name',
        'employment_date',
    ];

    protected $dates = [
        'employment_date',
    ];

    public function department()
    {
        return $this->belongsTo(Department::class);
    }
}
