<?php

namespace App\Models;

use App\Traits\Findable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    use HasFactory, Findable;

    protected $fillable = [
        'name',
        'salary_bonus_amount',
        'salary_bonus_percent',
    ];
}
