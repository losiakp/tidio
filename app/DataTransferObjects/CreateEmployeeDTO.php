<?php

namespace App\DataTransferObjects;

use App\Models\Employee;

final class CreateEmployeeDTO
{
    public $firstName;
    public $lastName;
    public $baseSalary;

    public function __construct(array $data)
    {

    }
}
