<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## Instalacja
- Projekt instalujemy zgodnie z dokumentacją Laravela
- Przykładowe dane w seedach (`php artisan migrate --seed`)

## Endpointy
- POST `/api/employees` dodawanie pracownika. Wymagane dane: department_id, base_salary, first_name, last_name. Opcjonalne: employment_date (jeśli nie podamy ustawi się "now") 
- GET `/api/employees` pobieranie pracowników
- GET `/api/salaries` endpoint z raportem płac pracowników. 
    - Filtry:
        - firstName
        - lastName
        - department
        - orderBy (Przykład: orderBy=employees.first_name|asc), dostępne sortowania:   
            - employees.first_name|asc
            - employees.first_name|desc
            - employees.last_name|asc
            - employees.last_name|desc
            - departments.name|asc
            - departments.name|desc
            - salaries.base|asc
            - salaries.base|desc
            - salaries.bonus|asc
            - salaries.bonus|desc
            - salaries.bonus_type|asc
            - salaries.bonus_type|desc
            - salaries.total|asc
            - salaries.total|desc
        

## Uwagi
- Kwoty są trzymane w integerach (można też w decimal, lecz osobiście wolę konwencję integerów)    
- Do filtrowania został użyty moduł napisany przezemnie (dużo wcześniej), który pozwala na wygodne zastosowanie filtrów, które są kompatybilne z [Fractalem](https://fractal.thephpleague.com/)
- Do zwrócenia danych w ednpoincie raportu został użyty wbudowany mechanizm Resource Collection, lecz normalnie użyłbym Transformerów z Fractala
- Pensje powinny być przeliczane pierwszego dnia każdego miesiąca, aby dane były aktualne (cron co miesiąc)
- Nie robiłem DDD bo zajęłoby to znacznie więcej czasu

