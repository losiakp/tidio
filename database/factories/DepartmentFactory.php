<?php

namespace Database\Factories;

use App\Models\Department;
use Illuminate\Database\Eloquent\Factories\Factory;

class DepartmentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Department::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->word,
        ];
    }

    public function amountBonus()
    {
        return $this->state(function (array $attributes) {
            return [
                'salary_bonus_amount' => $this->faker->randomElement([10000, 20000, 25000, 30000, 40000, 50000]),
            ];
        });
    }

    public function percentBonus()
    {
        return $this->state(function (array $attributes) {
            return [
                'salary_bonus_percent' => $this->faker->numberBetween(10, 15),
            ];
        });
    }
}
