<?php

namespace Database\Seeders;

use App\Factories\EmployeeFactory;
use App\Models\Department;
use App\Models\Employee;
use Illuminate\Database\Seeder;

class EmployeesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $departments = Department::all();

        $data = [
            [
                'first_name'      => 'Employee 1',
                'last_name'       => 'Test 1',
                'base_salary'     => '100000',
                'employment_date' => '2012-04-03 00:00:00',
            ],
            [
                'first_name'      => 'Worker 1',
                'last_name'       => 'Test 2',
                'base_salary'     => '300000',
                'employment_date' => '2010-04-03 00:00:00',
            ],
            [
                'first_name'      => 'Employee 2',
                'last_name'       => 'Test 3',
                'base_salary'     => '200000',
                'employment_date' => '2015-04-03 00:00:00',
            ],
            [
                'first_name'      => 'Worker 2',
                'last_name'       => 'Test 4',
                'base_salary'     => '100000',
                'employment_date' => '2004-04-03 00:00:00',
            ],
        ];

        $departments->each(function (Department $department, $key) use ($data) {
            (new EmployeeFactory())->create($department->id, $data[$key]['first_name'], $data[$key]['last_name'], $data[$key]['base_salary'], $data[$key]['employment_date']);
        });


    }
}
