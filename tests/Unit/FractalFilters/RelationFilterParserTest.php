<?php

namespace Tests\Unit\FractalFilters;

use App\Services\Filters\RelationFiltersParser;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RelationFilterParserTest extends TestCase
{
    public $includeString = 'assets,fieldTemplate.fields.fieldValues:product(1),tags';

    /** @test */
    public function it_returns_true_after_parse()
    {
        $relationFilterParser = new RelationFiltersParser($this->includeString);
        $this->assertTrue($relationFilterParser->parse());
    }

    /** @test */
    public function it_returns_array_of_requested_includes()
    {
        $relationFilterParser = new RelationFiltersParser($this->includeString);
        $relationFilterParser->parse();
        $count = count(explode(',', $this->includeString));

        $this->assertCount($count, $relationFilterParser->getRequestedIncludes());
    }
}
