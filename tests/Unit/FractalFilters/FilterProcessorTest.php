<?php

namespace Tests\Unit\FractalFilters;

use App\Models\Employee;
use App\Services\Filters\Drivers\EloquentDriver;
use App\Services\Filters\FiltersProcessor;
use App\Services\Filters\Kernel;
use Tests\TestCase;

class FilterProcessorTest extends TestCase
{
    public $filters = [
        'name'   => 1,
        'include' => 'product',
    ];

    /** @test
     */
    public function it_process_filters()
    {
        $builder = Employee::query();
        $processor = new FiltersProcessor(new EloquentDriver($builder, new Kernel()), $this->filters, 'employee');
        $processor->process();

        $builder->with($processor->applyRelationFilters());

        $this->assertStringContainsString('name', $builder->toSql());
    }
}
